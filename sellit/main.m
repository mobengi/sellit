//
//  main.m
//  sellit
//
//  Created by Marcos Ulises Tong Alvarez on 9/5/14.
//  Copyright (c) 2014 BigBidder. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
